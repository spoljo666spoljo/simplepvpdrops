package com.spoljo666spoljo.SimplePvPDrops;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Main extends JavaPlugin implements Listener {

	public static boolean update = false;
	public static String name = "";
	public static long size = 0;

	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(this, this);
		getCommand("simplepvp").setExecutor(new CmdExecutor(this));
		final File f = new File(getDataFolder().getAbsolutePath(), "config.yml");
		if (!f.exists()) {
			saveDefaultConfig();
			try {
				final FileWriter fw = new FileWriter(f);
				fw.write("# " + getDescription().getName() + " v" + getDescription().getVersion() + " by spoljo666spoljo" + "\n" + "\n");

				fw.write("# if true, Update-Checker will be enabled." + "\n");
				fw.write("update-checker: true" + "\n");

				fw.write("# If true, the drops will be removed after the delay." + "\n");
				fw.write("remove-drops: false" + "\n");

				fw.write("# Delay (in seconds) after which the drops will be removed." + "\n");
				fw.write("remove-drops-delay: 3" + "\n" + "\n");

				fw.write("deny-drops:" + "\n");

				fw.write("  # If true, players will not be able to drop items." + "\n");
				fw.write("  enabled: false" + "\n");

				fw.write("  # Message which will be shown if a player drops an item while the above is set to 'true'" + "\n");
				fw.write("  message: '&cYou can not drop items!'");

				fw.flush();
				fw.close();
			} catch (final IOException ex) {
				ex.printStackTrace();
			}
			if (getConfig().getBoolean("update-checker") == true) {
				final Updater updater = new Updater(this, "simplepvpdrops", getFile(), Updater.UpdateType.DEFAULT, true);
				update = updater.getResult() == Updater.UpdateResult.UPDATE_AVAILABLE;
				name = updater.getLatestVersionString();
				size = updater.getFileSize();
			}
		}
	}

	@EventHandler
	public void onPlayerDropItemEvent(final PlayerDropItemEvent e) {
		final Player p = e.getPlayer();
		final Item d = e.getItemDrop();
		if (getConfig().getBoolean("remove-drops", true)) {
			if (getConfig().getBoolean("deny-drops.enabled") == false) {
				new BukkitRunnable() {
					@Override
					public void run() {
						d.getWorld().playEffect(d.getLocation(), Effect.SMOKE, 1);
						d.getWorld().playSound(d.getLocation(), Sound.LAVA_POP, 1, 1);
						d.remove();
					}
				}.runTaskLater(this, 20 * getConfig().getInt("remove-drops-delay"));
			} else if (getConfig().getBoolean("deny-drops.enabled") == true) {
				d.remove();
			}
		}
		if (getConfig().getBoolean("deny-drops.enabled") == true) {
			e.setCancelled(true);
			final String msg = getConfig().getString("deny-drops.message");
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
		}
	}
}